(function() {
	"use strict";

	var sharedApi = {};
	var filterTags = [];
	var groupedFilterTags = {};
	var sortingOrders = ["newest", "alphabetical_az", "alphabetical_za"];
	var selectedSortingOrder = sortingOrders[0];
	var urlTemplate = "{path}.filtertags.{tags}.sortingOrder.{order}.html";
	var groupedUrlTemplate = "{path}.sortingOrder.{order}.html?source=checkboxes{checkboxesTags}";
	var urlTemplateWithoutFilters = "{path}.sortingOrder.{order}.html";

	sharedApi.getTemplateUrl = function() {
		if (filterTags.length) {
			return urlTemplate
				.replace("{tags}", encodeURIComponent(filterTags.join(".")))
				.replace("{order}", selectedSortingOrder);
		}
		return urlTemplateWithoutFilters.replace("{order}", selectedSortingOrder);
	};
	sharedApi.getGroupedTemplateUrl = function() {
		var groupedTags = "";
		if (groupedFilterTags) {
			groupedTags = buildGroupedTagsQuery();
			return groupedUrlTemplate
				.replace("{checkboxesTags}", groupedTags)
				.replace("{order}", selectedSortingOrder);
		}
		return urlTemplateWithoutFilters.replace("{order}", selectedSortingOrder);
	};

	function buildGroupedTagsQuery() {
		var result = "";
		Object.keys(groupedFilterTags).forEach(function(group) {
			result += "&ct-".concat(group).concat("=");
			result += encodeURIComponent(groupedFilterTags[group].join(","));
		});
		return result;
	}

	sharedApi.setFilterTags = function(tags) {
		filterTags = tags.map(function(tag) {
			return tag.replace(/\//g, ":");
		});
	};

	sharedApi.setSortingOrder = function(sortingOrder) {
		if (sortingOrders.includes(sortingOrder)) {
			selectedSortingOrder = sortingOrder;
		}
	};

	sharedApi.addFilterTag = function(tag, groupId) {
		var tagToAdd = tag.replace(/\//g, ":");

		if (!!groupId) {
			if (!groupedFilterTags.hasOwnProperty(groupId)) {
				groupedFilterTags[groupId] = [];
			}
			groupedFilterTags[groupId].push(tagToAdd);
		} else {
			if (!filterTags.includes(tagToAdd)) {
				filterTags.push(tagToAdd);
			}
		}
	};

	sharedApi.removeFilterTag = function(tag, groupId) {
		var tagToRemove = tag.replace(/\//g, ":");

		if (!!groupId && groupedFilterTags.hasOwnProperty(groupId)) {
			groupedFilterTags[groupId] = groupedFilterTags[groupId].filter(function(t) {
				return t !== tagToRemove;
			});
			if (groupedFilterTags[groupId].length === 0) {
				delete groupedFilterTags[groupId];
			}
		} else {
			filterTags = filterTags.filter(function(t) {
				return t !== tagToRemove;
			});
		}

	};

	sharedApi.trackFilters = function(data) {
		var tags = filterTags.filter(function(tag) {
			return tag !== "";
		});
		sharedApi.globalFilteredTags[data.componentPosition] = tags;

		if (tags.length) {
			var summaryTags = sharedApi.globalFilteredTags.reduce(function(a, b) {
				return a.concat(b);
			}, []);
			summaryTags = summaryTags.join(" | ");

			Cog.fireEvent("filters", "analytics", {
				componentName: data.componentName,
				componentPosition: data.componentPosition,
				filters: summaryTags
			});
		}
	};

	sharedApi.globalFilteredTags = [];

	Cog.registerStatic({
		name: "utils.filters",
		sharedApi: sharedApi,
		api: {}
	});
})();
