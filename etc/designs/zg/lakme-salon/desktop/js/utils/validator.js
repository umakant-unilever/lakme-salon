(function($) {
	"use strict";

	function errorMsg($holder, data) {
		return $holder.data(data) || $holder.find("[data-" + data + "]").data(data);
	}

	var rules = (function() {
		var all = [];

		return {
			add: function(query, fn) {
				all.push({query: query, fn: fn});
			},
			err: function($elm) {
				var valid, err = [];

				$.each(all, function(i, rule) {
					if ($elm.is(rule.query) || $elm.find(rule.query).length) {
						valid = rule.fn($elm);
						if (valid.msg) {
							err.push(valid.msg);
						}
					}
				});

				return err;
			}
		};
	}());

	rules.add("[required],.g-recaptcha", function($holder) {
		var $field = $holder.find("input, select, textarea");
		var valid = (function() {
			if ($field.is("[type=checkbox]")) {
				return $field.is(":checked");
			} else if ($field.filter("[type=radio]").length > 0) {
				return $field.filter(":checked").length > 0;
			} else {
				return $field.val() !== "";
			}
		}());

		return valid || {msg: errorMsg($holder, "required-error-message")};
	});

	rules.add("[data-min-age]", function($holder) {
		var $input = $holder.find("input");
		var config = $input.data("min-age") || {minAge: 0, errorMessage: ""};
		var today = new Date();
		var val = ($input.data("pickadate").get("select") || {}).obj || new Date();
		var age = today.getFullYear() - val.getFullYear();
		age = today.getMonth() - val.getMonth() < 0 || today.getDate() - val.getDate() < 0 ? (age - 1) : age;

		return age >= config.minAge || {msg: config.errorMessage.replace("{age}", config.minAge)};
	});

	rules.add("[pattern]", function($holder) {
		var $field = $holder.find("input");
		var regexp = new RegExp($field.attr("pattern"));
		return !$field.val().length || regexp.test($field.val()) || {msg: errorMsg($holder, "invalid-value-error-message")};
	});

	function field($holder) {
		var $field = $holder.find("input, select, textarea");
		var $msg;

		function error(err) {
			$msg = $msg || $("<span class=\"error-msg\">");
			$msg.text(err[0]);
			$holder.addClass("error");
			$holder.append($msg);
			setTimeout(function() {
				$msg.addClass("active");
			}, 0);
		}

		function success() {
			$holder.removeClass("error");
			if ($msg) {
				$msg.removeClass("active");
			}
		}

		function check() {
			var err = rules.err($holder);
			(err.length ? error : success)(err);
			return !err.length;
		}

		$field.on("blur, change", function() {
			check();
		});

		return {
			$holder: $holder,
			$field: $field.eq(0),
			check: check
		};
	}

	function validator($form, fieldsQuery) {
		var fields = [];
		fieldsQuery = fieldsQuery || [];

		function isValid() {
			var err = fields.filter(function(fld) {
				return !fld.check();
			});

			if (err.length) {
				err[0].$field.focus();
			}

			return !err.length;
		}

		$form.find(fieldsQuery.join(",")).each(function(i, elm) {
			fields.push(field($(elm)));
		});

		$form.attr("novalidate", "");
		$form.on("submit", function(event) {
			if (!isValid()) {
				event.preventDefault();
			} else if ($("html").hasClass("ua-ie-11")) {
				// In IE11, even if an input has no "name" attribute, its value is added to a request.
				// It's not expected by some endpoints and results in a 500 error (e.g. the sign-up form).
				var $datepickers = $(this).find(".reference-datepicker");

				$datepickers.each(function() {
					$(this).find(".datepicker-input").remove();
				});
			}
		});
	}

	Cog.registerStatic({
		name: "utils.validator",
		api: {},
		sharedApi: validator
	});
}(Cog.jQuery()));

