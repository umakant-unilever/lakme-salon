(function() {
	"use strict";

	var api = {},
		sharedApi = {};

	api.init = function() {
		var path = location.pathname.split("/");
		if (path.length > 2) {
			Cog.Cookie.create("country", path[1]);
			Cog.Cookie.create("language", path[2]);
		}
	};

	Cog.registerStatic({
		name: "utils.geolocation",
		api: api,
		sharedApi: sharedApi
	});

})(Cog.jQuery());
