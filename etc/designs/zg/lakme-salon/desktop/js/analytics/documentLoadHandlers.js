(function($) {
	"use strict";

	var api = {};
	var events;
	var utils;
	var ctConstants;

	api.init = function() {
		events = this.external.eventsDefinition.LOAD;
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		addEventListeners();
	};

	function addEventListeners() {
		if (isDigitalDataActive) {
			$(document).ready(addComponentsPositionNumber);
			Cog.addListener("virtualAgent", events.VIRTUAL_AGENT, virtualAgentLoad);
		}
	}

	function addComponentsPositionNumber() {
		$(".component").each(function(position) {
			$(this).attr("data-position", position);
		});
	}

	function virtualAgentLoad(event) {
		utils.pushComponent("Virtual Agent", event.eventData.componentPosition,
				ctConstants.engagement, ctConstants.interest);
		utils.addTrackedEvent(ctConstants.virtualagent, "Virtual Agent-Launch",
				ctConstants.engagement, ctConstants.interest);
	}

	Cog.registerStatic({
		name: "analytics.onLoadHandlers",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})(Cog.jQuery());
