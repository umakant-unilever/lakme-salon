(function($) {
	"use strict";

	var api = {};
	var sharedApi = {};
	var ctConstants;
	var events;

	api.init = function() {
		ctConstants = this.external.eventsDefinition.ctConstants;
		events = this.external.eventsDefinition.CLICK;
	};

	sharedApi.pushComponent = function(name, position, category, subcategory, attributes) {	//jshint ignore:line
		digitalData.component = [];
		digitalData.component.push({
			componentInfo: {
				componentID: name,
				componentName: name
			},
			category: {primaryCategory: category || ctConstants.engagement},
			subcategory: subcategory || ctConstants.read,
			attributes: attributes || {
				position: position,
				componentPosition: position,
				campaignWorkFlow: "",
				componentVariant: "defaultView",
				promoCreative:"",
				promoID: "",
				promoName: "",
				promoPosition: "",
				reviewVendorName: "",
				toolName: "",
				timestamp: new Date()
			}
		});
	};

	sharedApi.addTrackedEvent = function(action, label, category, subcategory, attributesObject) {	//jshint ignore:line
		var ev = {};
		ev.eventInfo = {
			type: ctConstants.trackEvent,
			eventAction: action,
			eventLabel: label,
			eventValue: 1
		};
		ev.category = {primaryCategory: category || ctConstants.engagement};
		ev.subcategory = subcategory || ctConstants.read;
		ev.attributes = attributesObject || {};

		if (typeof PubSub !== "undefined") {
			PubSub.publish("UDM", ev);
		}
	};

	sharedApi.createProduct = function(productData) {
		productData = productData || {};
		return {
			productInfo:{
				productID: productData.EAN || "",
				productName: productData.shortTitle || "",
				price: 0,
				brand: digitalData.siteInfo.localbrand,
				quantity: 0,
				sku: productData.sku || ""
			},
			category:{
				primaryCategory: productData.category || ""
			},
			price:{
				voucherCode: "voucher code"
			},
			attributes: {
				productPosition: 0,
				productVariants: productData.sizes || "",
				listPosition: 0,
				pcatName: "",
				prodRetailer: "",
				productBrand: digitalData.siteInfo.localbrand,
				productAward: "",
				productFindingMethod: ""
			}
		};
	};

	sharedApi.pushProduct = function(product) {
		digitalData.product.push(product);
	};

	sharedApi.pushCart = function(products) {
		digitalData.cart.item = products;
	};

	sharedApi.getComponentPosition = function($component) {
		return $component.data("position") || "";
	};

	sharedApi.setSearchData = function(keyword, resultNumber) {
		digitalData.page.pageInfo.onsiteSearchTerm = keyword;
		digitalData.page.pageInfo.onSiteSearch = resultNumber || "";
	};

	sharedApi.determineLinkTitle = function($target) {
		return $target.attr("title") ||
				$target.text() ||
				$target.parent().attr("title") || //case for carousel links
				"";
	};

	sharedApi.resolveListingProductEan = function($element) {
		return $element.closest(".listing-item").data("item-primarykey").split("/").pop();
	};

	sharedApi.resolveSearchProductEan = function($element) {
		return $element.closest(".searchResults-item").data("ean") || $element.data("ean");
	};

	sharedApi.trackLinks = function($links, data) {
		$links.on("click", function(e) {
			var $target = $(e.target);
			data = data || {};
			data.type = data.type || "";
			data.componentName = data.componentName || "";
			data.componentPosition = data.componentPosition || "";
			data.href = $target.attr("href") || $target.parent().attr("href");
			data.label = sharedApi.determineLinkTitle($target);

			if (data.type === ctConstants.product) {
				if (data.componentName === ctConstants.listing) {
					data.ean = this.resolveListingProductEan($target);
				} else if (data.componentName === ctConstants.searchResults) {
					data.ean = this.resolveSearchProductEan($target);
				}
			}

			Cog.fireEvent("link", events.LINK_CLICK, data);
		});
	};

	Cog.registerStatic({
		name: "analytics.utils",
		sharedApi: sharedApi,
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			}]
	});
})(Cog.jQuery());
