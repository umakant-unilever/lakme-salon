(function($) {
	"use strict";

	var api = {};
	var events;
	var utils;
	var ctConstants;
	var continueButtonClass = ".shopping-bag-continue-link";
	var cartViewButtonClass = ".shoppable-view-cart-button";
	var checkoutButtonClass = ".shopping-bag-checkout-link";
	var closeMiniBagClass = ".shoppable-checkout-close-button";
	//keeping the cart state (needed for identifying deleted products)
	var previousCart = [];

	api.init = function() {
		events = this.external.eventsDefinition;
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		addEventListeners();
	};

	function addEventListeners() {
		if (isDigitalDataActive) {
			Cog.addListener("shoppable", events.OTHER.SHOPPABLE_INIT, addShoppableListeners);
			Cog.addListener("shoppable", events.OTHER.SHOPPABLE_PURCHASE, trackPurchase);
			Cog.addListener("shoppable", events.CLICK.SHOPPABLE_BIN_CLICK, binClick);
		}
	}

	function addShoppableListeners() {
		if (typeof jQuery !== "undefined" && typeof jQuery.Topic !== "undefined") {
			//we use jQuery object because this is shoppable's jQuery with Topic implementation
			jQuery.Topic("ADD_TO_CART").subscribe(addToCartClick);
			jQuery.Topic("FOUND_CART").subscribe(saveCartState);
			jQuery.Topic("REMOVE_FROM_CART").subscribe(removeFromCartClick);
			jQuery.Topic("OPEN_CART").subscribe(openCartClick);
			jQuery.Topic("CLOSE_CART").subscribe(closeCartClick);

			$(continueButtonClass).click(continueClick);
			$(checkoutButtonClass).click(checkoutClick);
			//button shows in markup after first bag opening, so this way to handle event is necessary
			$(document).on("click", cartViewButtonClass, cartViewClick);
			$(document).on("click", closeMiniBagClass, closeCartClick);
		}
	}

	function binClick(event) {
		var analyticsProduct = utils.createProduct();
		var platformProduct = allProducts[event.eventData.product] || {};
		var eventLabel = "Online - " + platformProduct.shortTitle;
		var eventData = {
			action: ctConstants.purchase,
			label: eventLabel,
			category: ctConstants.conversion,
			subCategory: ctConstants.lead
		};
		var products = [];
		analyticsProduct.productInfo.productName = platformProduct.shortTitle;
		analyticsProduct.productInfo.productID = platformProduct.EAN;
		analyticsProduct.productInfo.productBrand = digitalData.siteInfo.localbrand;
		analyticsProduct.productInfo.price = platformProduct.productPrice;
		analyticsProduct.productInfo.sku = platformProduct.sku;
		analyticsProduct.category.primaryCategory = platformProduct.category;
		analyticsProduct.attributes.pcatName = platformProduct.category;
		analyticsProduct.attributes.productVariants = platformProduct.sizes;
		products.push(analyticsProduct);
		pushCartEvent(products, eventData);
	}

	function saveCartState(currentCart) {
		previousCart = parseCart(currentCart);
	}

	function addToCartClick(currentCart) {
		currentCart = JSON.parse(currentCart);
		var products = [];
		var eventLabel = currentCart.config.data.items[0].name;
		var event = {
			action: ctConstants.addtoCart,
			label: eventLabel,
			category: ctConstants.conversion,
			subCategory: ctConstants.lead
		};

		$.each(currentCart.config.data.items, function(index, item) {
			var product = utils.createProduct();
			product.productInfo.productID = item.variances[0].upc;
			product.productInfo.productName = item.name;
			product.productInfo.price = item.price;
			product.productInfo.brand = item.brand;
			product.productInfo.quantity = item.qty;
			product.attributes.prodRetailer = item.variances[0].merchant;
			product.attributes.productBrand = item.brand;

			product.attributes.productVariants = item.variances.map(function(elem) {
				return elem.size;
			}).join(" | ");
			products.push(product);
		});
		pushCartEvent(products, event);
	}

	function removeFromCartClick(currentCart) {
		var products = parseCart(currentCart);
		products = _.differenceWith(previousCart, products, _.isEqual);

		var productNames = products.map(function(elem) {
			return elem.productInfo.productName;
		}).join(" | ");

		var eventLabel = "Remove items - " + productNames;
		var event = {
			action: ctConstants.removecart,
			label: eventLabel,
			category: ctConstants.conversion,
			subCategory: ctConstants.diversion
		};

		pushCartEvent(products, event);
	}

	function openCartClick() {
		toggleCartClick("Open");
	}

	function closeCartClick() {
		toggleCartClick("Close");
	}

	function cartViewClick() {
		utils.pushComponent("Shoppable", "");
		utils.addTrackedEvent(ctConstants.viewBag,
			document.title,
			ctConstants.conversion,
			ctConstants.read);
	}

	function toggleCartClick(eventLabel) {
		utils.pushComponent("Shoppable", "");
		utils.addTrackedEvent(ctConstants.miniBag,
			eventLabel,
			ctConstants.conversion,
			ctConstants.interest);
	}

	function continueClick() {
		utils.pushComponent("Shoppable","");
		utils.addTrackedEvent(ctConstants.continueShopping,
			"Continue shopping",
			ctConstants.conversion,
			ctConstants.interest);
	}

	function checkoutClick() {
		var eventLabel = "Checkout click";
		var event = {
			action: ctConstants.checkoutenhanced,
			label: eventLabel,
			category: ctConstants.conversion,
			subCategory: ctConstants.win
		};
		pushCartEvent(previousCart, event);
	}

	function trackPurchase(event) {
		var eventLabel = event.eventData.orderNumber;
		var products = [];
		digitalData.transaction = [];
		digitalData.transaction.item = [];
		digitalData.transaction.total = {};
		digitalData.transaction.transactionID = event.eventData.orderNumber;
		digitalData.transaction.total.voucherCode = "voucher Code";

		$.each(event.eventData.products, function(index, item) {
			var product = utils.createProduct();
			product.productInfo.productID = item.upc;
			product.productInfo.productName = item.product_name;
			product.productInfo.sku = item.sku;
			product.productInfo.quantity = item.quantity;
			product.price.basePrice = item.item_total;
			product.productInfo.voucherCode = "voucher Code";
			product.attributes.prodRetailer = item.merchant;
			product.attributes.productBrand = item.brand;
			product.attributes.onlineStore = "Online Store";
			product.attributes.paymentMethod = "Payment Method";

			products.push(product);
		});

		digitalData.transaction.item = products;
		utils.pushComponent("Shoppable", "");
		utils.addTrackedEvent(ctConstants.purchaseenhanced,
			eventLabel,
			ctConstants.conversion,
			ctConstants.win, {
				nonInteractive: {
					nonInteraction: 1
				}
			});
		utils.addTrackedEvent(ctConstants.ordersummary,
			eventLabel,
			ctConstants.conversion,
			ctConstants.win, {
				nonInteractive: {
					nonInteraction: 1
				}
			});
	}

	function pushCartEvent(cartProducts, event) {
		digitalData.cart = [];
		digitalData.cart.item = [];
		utils.pushCart(cartProducts);
		utils.pushComponent("Shoppable", "");
		utils.addTrackedEvent(event.action, event.label, event.category, event.subCategory);
	}

	function parseCart(cart) {
		cart = JSON.parse(cart);
		var products = [];
		if (cart.merchants) {
			$.each(cart.merchants, function(index, merchant) {
				$.each(merchant.items, function(index, item) {
					var product = utils.createProduct();
					product.productInfo.productID = item.upc;
					product.attributes.prodRetailer = merchant.name;
					product.productInfo.productName = item.name;
					product.productInfo.price = item.price;
					product.productInfo.brand = item.brand;
					product.productInfo.quantity = item.qty;
					product.attributes.productBrand = item.brand;

					products.push(product);
				});
			});
		}
		return products;
	}

	Cog.registerStatic({
		name: "analytics.shoppableHandler",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
}(Cog.jQuery()));
