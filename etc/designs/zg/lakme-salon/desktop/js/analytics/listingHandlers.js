(function($) {
	"use strict";

	var api = {};
	var utils;
	var ctConstants;
	var attributes;

	api.init = function() {
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		attributes = {
			nonInteractive: {
				nonInteraction: 1
			}
		};
		addEventListeners();
	};

	function addEventListeners() {
		if (isDigitalDataActive) {
			Cog.addListener("filters", "analytics", onFilter);
			Cog.addListener("listing", ctConstants.productImpression, onInstantImpresssion);
			window.onload = onLoadImpresssion;
		}
	}

	function onFilter(event) {
		utils.pushComponent(event.eventData.componentName, event.eventData.componentPosition);
		utils.addTrackedEvent(ctConstants.filter, event.eventData.filters, ctConstants.engagement, ctConstants.interest);
	}

	function onInstantImpresssion(event) {
		var productNames = [];

		$.each(event.eventData.products, function() {
			var ean;

			if (event.eventData.componentName === ctConstants.listing) {
				ean = utils.resolveListingProductEan($(this));
			} else if (event.eventData.componentName === ctConstants.searchResults) {
				ean = utils.resolveSearchProductEan($(this));
			}

			if (allProducts[ean]) {
				var product = utils.createProduct(allProducts[ean]);

				utils.pushProduct(product);
				productNames.push(product.productInfo.productName);
			}
		});

		utils.pushComponent(event.eventData.componentName, event.eventData.componentPosition);
		utils.addTrackedEvent(ctConstants.productImpression, productNames, ctConstants.custom, ctConstants.lead, attributes);
	}

	function onLoadImpresssion() {
		if (digitalData.onLoadProductNames) {
			utils.addTrackedEvent(ctConstants.productImpression, digitalData.onLoadProductNames,
					ctConstants.custom, ctConstants.lead, attributes);
		}
	}

	Cog.registerStatic({
		name: "analytics.listingHandlers",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})(Cog.jQuery());
