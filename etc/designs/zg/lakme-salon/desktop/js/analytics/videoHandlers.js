(function() {
	"use strict";

	var api = {};
	var utils;
	var ctConstants;

	api.init = function() {
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		addEventListeners();
	};

	function addEventListeners() {
		if (isDigitalDataActive) {
			Cog.addListener("video", ctConstants.videoPlays, playHandler, {disposable: true});
			Cog.addListener("video", ctConstants.videoProgress, progressHandler);
			Cog.addListener("video", ctConstants.videoCompletes, completeHandler, {disposable: true});
			Cog.addListener("video", ctConstants.VideoFastForwarded, fastforwardHandler);
		}
	}

	function playHandler(event) {
		var label = createLabel([ctConstants.youtube, event.eventData.videoTitle, event.eventData.videoId]);
		utils.pushComponent("Video", event.eventData.componentPosition, ctConstants.engagement, ctConstants.interest);
		utils.addTrackedEvent(ctConstants.videoPlays, label, ctConstants.engagement, ctConstants.interest);
	}

	function progressHandler(event) {
		var label = createLabel([event.eventData.videoTitle, "Video Progress", event.eventData.progress]);
		utils.pushComponent("Video", event.eventData.componentPosition, ctConstants.custom, ctConstants.read);
		digitalData.video.push({videoid: event.eventData.videoId});
		utils.addTrackedEvent(ctConstants.videoProgress, label, ctConstants.custom, ctConstants.read);
	}

	function completeHandler(event) {
		var label = createLabel([ctConstants.youtube, event.eventData.videoTitle, event.eventData.videoId]);
		utils.pushComponent("Video", event.eventData.componentPosition, ctConstants.engagement, ctConstants.interest);
		utils.addTrackedEvent(ctConstants.videoCompletes, label, ctConstants.engagement, ctConstants.interest);
	}

	function fastforwardHandler(event) {
		var label = createLabel([event.eventData.videoTitle, event.eventData.videoId]);
		utils.pushComponent("Video", event.eventData.componentPosition, ctConstants.engagement, ctConstants.other);
		utils.addTrackedEvent(ctConstants.VideoFastForwarded, label, ctConstants.engagement, ctConstants.other);
	}

	function createLabel(elements) {
		return elements.filter(Boolean).join(" - ");
	}

	Cog.registerStatic({
		name: "analytics.videoHandlers",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})();
