(function() {
	"use strict";

	var api = {};
	var events;
	var utils;
	var ctConstants;

	api.init = function() {
		events = this.external.eventsDefinition.SUBMIT;
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		addEventListeners();
	};

	function addEventListeners() {
		if (isDigitalDataActive) {
			Cog.addListener("form", events.FORM, formHandler);
		}
	}

	function formHandler(event) {
		utils.pushComponent("Form", event.eventData.componentPosition);
		utils.addTrackedEvent(ctConstants.forms, event.eventData.label);
	}

	Cog.register({
		name: "analytics.submitHandlers",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})();
