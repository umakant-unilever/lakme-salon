(function($) {
	"use strict";

	var api = {};

	var options = {
		button: {
			selector: "imageGallery-with-zoom__button"
		},
		zoom: {
			selector: "imageGallery-with-zoom__overlay"
		},
		img: {
			selector: "imageGallery-with-zoom__image"
		}
	};

	function GalleryZoom($item) {
		this.$item = $item;
		var zoomButton = '<button class="' + options.button.selector + '"></button>';
		var $zoomButton = $(zoomButton).appendTo(this.$item);
		var $galleryView = this.$item.find(".imageGallery-view");
		var overlayHTML = '<div class="' + options.zoom.selector + '"></div>';
		var $overlay;

		var closeZoom = function(evt) {
			if (evt.type === "keyup" && evt.keyCode !== 27) {
				return; // if event type is keyup, close on esc key only.
			}
			var duration = getDuration($overlay);
			$(window).off("resize", closeZoom); // detach event listener
			$(document).off("keyup", closeZoom); // detach event listener
			$overlay.off();
			$overlay.removeClass("is-active"); // turn off the 'close' cursor
			$overlay.css("opacity", 0); // fade out overlay
			setTimeout(function() {
				$overlay.remove(); // remove completely, create new $overlay for each button press)
			}, duration);
		};
		var loadImage = function(ops) {
			var img = new Image();
			img.src = ops.src;
			img.setAttribute("alt", ops.alt || "");
			img.setAttribute("aria-visible", "true");
			img.addEventListener("load", function() {
				var imgW = img.naturalWidth;
				var imgH = img.naturalHeight;
				var winW = $(window).width();
				var $img = $(img);

				img.setAttribute("height", imgH);
				img.setAttribute("width", imgW);
				$overlay.addClass("is-active").append($img);
				$img
					.css({minWidth: imgW + "px"})
					.addClass(options.img.selector);

				if ($img.css("position") === "static") {
					// for mobile we put the image into the centre
					$overlay.scrollLeft((imgW / 2) - (winW / 2));
				} else {
					// else scroll image with mouse position
					scrollImage($img);
				}
			});
		};
		var scrollImage = function($img) {
			var imgH = $img.height();
			var winH = $(window).height();
			var a;
			var b;
			var c;
			var mouseMove = function(e) {
				// method copied from Dove.us
				a = imgH - winH;
				b = 1 - e.clientY / winH;
				c = -a + b * a;
				$img.css("top", c + "px");
			};

			$overlay.on("mousemove", mouseMove);

		};
		var getAlt = function($img) {
			var alt = $img.attr("alt") || document.title;
			return alt;
		};
		var getDuration = function($el) {
			// get the current opacity transition duration from the css
			// and convert it to ms if it's not already
			var i = 10;
			var duration = $el.css("transition-duration");
			if (duration && duration.indexOf(" ") === -1) {
				i = parseFloat(duration);
			}
			if (duration && duration.indexOf("ms") === -1) {
				i = i * 1000;
			}
			return i;
		};
		var launchZoom = function() {
			var $img = $galleryView.find("img");
			var src = $img.attr("src");
			var alt = getAlt($img);

			// todo: remove this and add to CSS
			// var styles = $(".zoomStyles").length;
			// if (!styles) {
			//	$("body").append(CSS)
			// }

			if (src) {// something is wrong, no image found, ignoring request to zoom
				$overlay = $(overlayHTML)
					.appendTo("body")
					.addClass(options.zoom.selector)
					.on("click", closeZoom);

				$(window).on("resize", closeZoom);
				$(document).on("keyup", closeZoom);

				requestAnimationFrame(function() {
					$overlay.css("opacity", 1);
					loadImage({src: src, alt: alt});
				});
			}
		};

		$zoomButton.on("click", launchZoom);
	}

	api.onRegister = function(scope) {
		new GalleryZoom(scope.$scope);
	};

	Cog.registerComponent({
		name: "gallery-zoom",
		api: api,
		selector: ".imageGallery-with-zoom"
	});
})(Cog.jQuery());
