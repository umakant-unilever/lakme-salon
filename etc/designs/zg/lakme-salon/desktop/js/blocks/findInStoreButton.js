(function() {
	"use strict";

	var api = {};
	var url;

	function FindInStoreButton($el) {
		this.$el = $el;
		this.$button = $el.find("a");
		this.basePath = url.removeParameters(this.$button.attr("href"));

		this.init();
	}

	FindInStoreButton.prototype.init = function() {
		if (this.$el.parents(".quickview-container").length) {
			Cog.addListener("variantList", "variantChanged", function(e) {
				var ean = e.eventData.ean;
				var path = url.addOrUpdateQueryParam(this.basePath, "ean", ean);
				this.$button.attr("href", path);
			}.bind(this));
		}
	};

	api.onRegister = function(scope) {
		url = this.external.url;
		new FindInStoreButton(scope.$scope);
	};

	Cog.registerComponent({
		name: "findInStoreButton",
		api: api,
		selector: ".reference-find-in-store-button",
		requires: [
			{
				name: "utils.url",
				apiId: "url"
			}
		]
	});
})();
