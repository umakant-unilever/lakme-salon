(function($) {

	"use strict";

	var api = {};

	function BackStageTipArticles($el) {
		this.$el = $el;
		console.log(this.$el);
		this.$el.header = $("#header");
		this.$el.on("click", function() {
			console.log("ff");
			var sectionType = this.$el.data("sectiontype");
			var $scrollToSection = document.querySelector("div." + sectionType);
			var elemTop = $scrollToSection.offsetTop;
			var headerHeight = this.$el.header.height();
			console.log(headerHeight);
			console.log(sectionType);
			console.log(elemTop);
			$("body,html").animate({
				scrollTop: elemTop - headerHeight + 15
			});
		}.bind(this));
	}

	api.onRegister = function(scope) {
		new BackStageTipArticles(scope.$scope);
	};

	Cog.registerComponent({
		name: "backStageTipArticles",
		api: api,
		selector: ".container_item"
	});

}(Cog.jQuery()));
