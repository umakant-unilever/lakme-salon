(function() {
	"use strict";

	var api = {};

	api.onRegister = function(scope) {
		var $element = scope.$scope,
			$mainParentBox = $element.closest(".buy-it-now-shopalyst-provider");

		$element.on("click", function() {
			$mainParentBox.addClass("shopalyst-active");
		});
	};

	Cog.registerComponent({
		name: "buy-it-now-shopalyst",
		api: api,
		selector: ".shopalyst-btn"
	});
})(Cog.jQuery());
