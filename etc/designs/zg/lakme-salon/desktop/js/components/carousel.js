/**
 * Carousel
 */

(function($) {
	"use strict";

	var api = {};
	var throttleTime = 150;
	var analyticsDef;
	var analyticsUtils;
	var constants = {
		SWIPE_LEFT: 2,
		SWIPE_RIGHT: 4
	};

	function Carousel($el, status) {
		this.$el = $el;
		this.$slidesList = this.$el.find(".carousel-slides");
		this.$slides = this.$slidesList.find(".carousel-slide");
		this.$links = this.$slides.find("a");
		this.$controls = this.$el.find(".carousel-nav-item");
		this.$btnPrev = this.$el.find(".carousel-nav-prev");
		this.$btnNext = this.$el.find(".carousel-nav-next");

		this.slidesMaxHeight = this.$slidesList.data("height");
		this.delay = this.$slidesList.data("delay") * 1000 || 0;
		this.autoRotate = this.$slidesList.data("rotate");
		this.currentIndex = 0;
		this.status = status;
		this.hammer = new Hammer(this.$el.get(0));
		this.componentPosition = analyticsUtils.getComponentPosition($el);

		this.$slides.hide().first().show();
		this.bindUIEvents();
		this.handleAutoRotate(this.currentIndex);
		this.resizeView();
	}

	Carousel.prototype = {
		bindUIEvents: function() {
			analyticsUtils.trackLinks(this.$links, {
				componentName: analyticsDef.CLICK.CAROUSEL_CLICK,
				componentPosition: this.componentPosition
			});

			this.$controls.on("click", "a", function(e) {
				var $parent = $(e.target).parent();
				var index = this.$controls.index($parent);

				e.preventDefault();
				this.showSlide(index);
				this.trackNavigation(index + 1);
			}.bind(this));

			this.$btnPrev.on("click", "a", function(e) {
				e.preventDefault();
				this.trackNavigation("PREVIOUS");
				this.showPrevious();
			}.bind(this));

			this.$btnNext.on("click", "a", function(e) {
				e.preventDefault();
				this.trackNavigation("NEXT");
				this.showNext();
			}.bind(this));

			this.$slides.on("click", "img", function(e) {
				this.trackImageClick(e);
			}.bind(this));

			this.hammer.on("swipe", function(e) {
				if (e.offsetDirection === constants.SWIPE_RIGHT) {
					this.showPrevious();
				} else if (e.offsetDirection === constants.SWIPE_LEFT) {
					this.showNext();
				}
			}.bind(this));

			$(window).resize($.throttle(throttleTime, function() {
				this.resizeView();
			}.bind(this)));

			this.$btnNext.on("click", function() {
				this.trackNavigation("NEXT");
			}.bind(this));

			this.$btnPrev.on("click", function() {
				this.trackNavigation("PREVIOUS");
			}.bind(this));
		},

		showSlide: function(nextIndex) {
			clearTimeout(this.timerId);

			var $current = this.$slides.eq(this.currentIndex);

			nextIndex = (nextIndex < 0) ? (this.$slides.length - 1) :
				((nextIndex >= this.$slides.length) ? 0 : nextIndex);

			if (Number(nextIndex) !== Number(this.currentIndex)) {
				this.$slides.stop(true, true);

				$current.removeClass("is-active").fadeOut(function() {
					$(this).removeClass("is-active");
					this.$controls.removeClass("is-active").eq(nextIndex).addClass("is-active");
				}.bind(this));

				this.$slides.eq(nextIndex).fadeIn(function() {
					$(this).addClass("is-active");
				});
			}

			this.currentIndex = nextIndex;
			this.handleAutoRotate(nextIndex);
		},

		showNext: function() {
			this.showSlide(this.currentIndex + 1);
		},

		showPrevious: function() {
			this.showSlide(this.currentIndex - 1);
		},

		resizeView: function() {
			if (this.status.isAuthor()) {
				return;
			}
			this.$el.removeClass("is-ready");
			var maxHeight = 0;
			this.$slides.each(function() {
				var height = Math.round($(this).height());
				maxHeight = height > maxHeight ? height : maxHeight;
			});

			if (this.slidesMaxHeight) {
				if (maxHeight > this.slidesMaxHeight) {
					maxHeight = this.slidesMaxHeight;
				}
			}

			this.$slidesList.css("height", maxHeight);
			this.$el.addClass("is-ready");
		},

		handleAutoRotate: function(index) {
			if (this.delay > 0 && this.autoRotate === true) {
				this.timerId = setTimeout(function() {
					this.showSlide(index + 1);
				}.bind(this), this.delay);
			}
		},

		trackNavigation: function(navDirect) {
			Cog.fireEvent("carousel", analyticsDef.CLICK.CAROUSEL_CLICK, {
				query: navDirect,
				componentPosition: this.componentPosition
			});
		},

		trackImageClick: function(e) {
			Cog.fireEvent("image", analyticsDef.CLICK.IMAGE_CLICK, {
				altText: $(e.target).attr("alt"),
				componentPosition: this.componentPosition
			});
		}
	};

	api.onRegister = function(scope) {
		analyticsDef = this.external.eventsDefinition;
		analyticsUtils = this.external.utils;

		new Carousel(scope.$scope, this.external.status);
	};

	Cog.registerComponent({
		name: "carousel",
		api: api,
		selector: ".carousel",
		requires: [
			{
				name: "utils.status",
				apiId: "status"
			},
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})(Cog.jQuery());
