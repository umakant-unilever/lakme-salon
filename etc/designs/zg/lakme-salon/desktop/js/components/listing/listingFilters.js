/* global JSON */
(function($) {
	"use strict";

	var filtersHandler, compose, ctConstants, analyticsUtils;

	function filter($holder, data) {
		data = parseData(data);
		var $form = $holder.find(".filter-form");
		var $quantity = $form.find(".filter-quantity").eq(0);
		var $parentSelectWrapper = $form.find(".filter-category");
		var $childSelectWrapper = $form.find(".filter-subcategory");
		var $orderSelectWrapper = $form.find(".filter-order");
		var $parentSelect = $parentSelectWrapper.find("select");
		var $childSelect = $childSelectWrapper.find("select");
		var $orderSelect = $orderSelectWrapper.find("select");
		var $listing = $holder.next(".listing");

		if (!$listing.length) {
			$listing = $holder.next().find(".listing");
		}

		if (!$listing.length) {
			// if there is no listing found as next element, get a first listing on a page
			$listing = $(".listing").first();
		}

		var config = jsonConfig($listing);
		var componentPosition = analyticsUtils.getComponentPosition($holder);

		updateQuantity(config.itemsCount);

		function index(arr, fn) {
			return arr.reduceRight(function(a, b, i) {
				return fn(b) ? i : a;
			}, -1);
		}

		function selectChangeCategory($holder, fn) {
			var $select = $holder.find("select");

			$select.on("change", function() {
				if ($childSelect[0].selectedIndex === 0) {
					// if sub category is set to default then submit category change
					filtersHandler.setFilterTags([$childSelect.val(), $parentSelect.val()]);
					Cog.fireEvent("listingFilters", "filterUpdate");

					filtersHandler.trackFilters({
						componentName: "Listing Filters",
						componentPosition: componentPosition
					});

					fn($select.val());
				} else {
					// otherwise reset the subcategory and submit
					fn($select.val());
					$childSelect.val("").change(); // reset subcategory
				}
			});
		}

		function selectChangeSubCategory($holder, fn) {
			var $select = $holder.find("select");

			$select.on("change", function() {
				filtersHandler.setFilterTags([$childSelect.val(), $parentSelect.val()]);
				Cog.fireEvent("listingFilters", "filterUpdate");

				filtersHandler.trackFilters({
					componentName: "Listing Filters",
					componentPosition: componentPosition
				});

				fn($select.val());
			});
		}

		function selectChangeOrder($holder, fn) {
			var $select = $holder.find("select");

			$select.on("change", function() {
				filtersHandler.setSortingOrder($orderSelect.val());
				fn($select.val());
			});
		}

		function selectUpdate($holder) {
			var option = document.createElement("option");
			var $select = $holder.find("select");

			return function(data) {
				$select.html("");

				if (data.length === 0 || $select.length === 0) {
					$holder.hide();
					return;
				}

				var frag = document.createDocumentFragment();
				$holder.addClass("active");
				data.forEach(function(val) {
					var opt = option.cloneNode();
					opt.value = val.value;
					opt.innerHTML = val.label;
					frag.appendChild(opt);
				});
				$select[0].appendChild(frag);
				$select[0].value = "";
			};
		}

		function parseData(data) {
			function all() {
				return data
					.reduce(function(a, b) {
						return b.tags && b.tags.constructor === Array ? a.concat(b.tags) : a;
					}, [])
					.filter(function(val, i, arr) {
						return index(arr, function(v) {
							return v.label === val.label && val.value === v.value;
						}) === i;
					});
			}

			return data.map(function(val) {
				return !val.tags ? {label: val.label, value: "", tags: all()} : val;
			});
		}

		function jsonConfig($elm) {
			return JSON.parse($elm.find("script[type='application/json']").eq(0).text());
		}

		function childBasedOnParent(selected) {
			var i = index(data, function(val) {
				return val.value === selected;
			});
			return data[i].tags;
		}

		function ajaxCall() {
			var cache = {};

			return function() {
				var formAction = "";
				if ($parentSelectWrapper.length === 0) {
					formAction = filtersHandler.getGroupedTemplateUrl();
				} else {
					// when Listing Filters Component is not in "Order only" mode, it doesn't support integration
					// with Listing Check Boxes Filters
					formAction = filtersHandler.getTemplateUrl();
				}
				formAction = formAction.replace("{path}", config.path);
				$form.attr("action", formAction);

				if (cache[formAction]) {
					return cache[formAction].promise;
				}

				$quantity.hide();
				$form.addClass("loading");
				return $.ajax($form.attr("action"), {
						method: $form.attr("method") || "get"
					})
					.always(function() {
						$form.removeClass("loading");
					})
					.done(function(data) {
						cache[formAction] = {
							data: data,
							promise: $.Deferred().resolve(data)
						};
						return data;
					});
			};
		}

		function updateQuantity(qtd) {
			var qtdText = $quantity.text().replace(/\b\d+\b/, qtd);
			$quantity.text(qtdText);
			$quantity.addClass("active");
		}

		function result(promise) {
			return promise
				.done(function(data) {
					var config = jsonConfig($(data));
					$listing
						.html(config.itemsCount ? data : "")
						.removeClass("initialized");
					Cog.init({$element: $listing})
						.then(function() {
							Cog.fireEvent("listingFilters", "selectChanged");
							updateQuantity(config.itemsCount);
						});
				})
				.fail(function() {
					$listing.html("");
					updateQuantity(0);
				});
		}

		function preventDefault(e) {
			e.preventDefault();
		}

		function init() {
			var ajax = ajaxCall();
			var updateChildSelect = selectUpdate($childSelectWrapper);
			var updateParentSelect = selectUpdate($parentSelectWrapper);

			selectChangeCategory($parentSelectWrapper, compose(result, ajax, updateChildSelect, childBasedOnParent));
			selectChangeSubCategory($childSelectWrapper, compose(result, ajax));
			selectChangeOrder($orderSelectWrapper, compose(result, ajax));

			$form.on("submit", compose(result, ajax, preventDefault));

			Cog.addListener("listingCheckBoxesFilter", "filterUpdate", function() {
				$parentSelect.children(":first-child").prop("selected", true);
				$childSelect.children(":first-child").prop("selected", true);
			});

			updateChildSelect(childBasedOnParent(""));
			updateParentSelect(data);
		}

		init();
	}

	var api = {
		onRegister: function(scope) {
			filtersHandler = filtersHandler || this.external.filters;
			compose = compose || this.external.compose;
			ctConstants = this.external.eventsDefinition.ctConstants;
			analyticsUtils = this.external.utils;

			var $json = scope.$scope.find("script[type='application/json']");
			if ($json.length) {
				var data = JSON.parse($json.text());
				filter(scope.$scope, data);
			}
		}
	};

	Cog.registerComponent({
		name: "listingFilters",
		api: api,
		selector: ".listingFilters",
		requires: [
			{
				name: "utils.filters",
				apiId: "filters"
			},
			{
				name: "utils.compose",
				apiId: "compose"
			},
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})(Cog.jQuery());
