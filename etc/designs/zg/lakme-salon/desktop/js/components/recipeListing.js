(function($) {
	"use strict";

	var api = {};
	var nextRecipes;

	function RecipeListing($el) {
		this.$componentContent = $el.children().first();
		this.$showMoreBtn = $el.find(".recipeListing-show-more-btn");
		this.$list = this.$componentContent.find(".recipeListing-list");
		this.itemsToShow = parseInt(this.$componentContent.data("items-to-show"), 10);
		this.getRecipesUrl = this.$showMoreBtn.data("morepath");
		this.offset = this.itemsToShow;
		this.$sortDropdown = this.$componentContent.find("#sortOrder");
		this.$filterDropdowns = this.$componentContent.find(".recipeListing-main-filters select");
		this.sorting = this.$sortDropdown.children(":selected").val();
		this.filters = "";

		if (this.$showMoreBtn.length) {
			this.fetchNextRecipes();
		}

		this.bindUIEvents();
	}

	RecipeListing.prototype.bindUIEvents = function() {
		this.$showMoreBtn.on("click", function() {
			this.$list.append(nextRecipes);
			Cog.init(this.$list);
			Cog.fireEvent("kritique", "reloadInlineRatings");
			this.fetchNextRecipes();
		}.bind(this));

		this.$sortDropdown.on("change", function() {
			this.sorting = this.$sortDropdown.children(":selected").val();
			this.refreshListState();
		}.bind(this));

		this.$filterDropdowns.on("change", function() {
			this.filters = this.$filterDropdowns.children(":selected")
				.toArray()
				.filter(function(elem) {
					return elem.hasAttribute("value");
				})
				.map(function(elem) {
					return $(elem).val();
				}).join(",");

			this.refreshListState();
		}.bind(this));
	};

	RecipeListing.prototype.fetchNextRecipes = function(callback) {
		var url = this.getRecipesUrl + "?o=" + this.offset + "&s=" + this.sorting;
		url += this.filters ? "&t=" + this.filters : "";

		$.get(url, function(data) {
			if (!data) {
				nextRecipes = "";
				this.$showMoreBtn.addClass("is-hidden");
			} else {
				nextRecipes = data;
				this.$showMoreBtn.removeClass("is-hidden");
			}

			this.offset += this.itemsToShow;

			if (typeof callback === "function") {
				callback();
			}
		}.bind(this));
	};

	// Set initial state with currently selected filters and sorting
	RecipeListing.prototype.refreshListState = function() {
		this.offset = 0;

		this.fetchNextRecipes(function() {
			this.$list.html(nextRecipes);
			Cog.init(this.$list);
			Cog.fireEvent("kritique", "reloadInlineRatings");

			// Get the next batch of recipes that will be appended on "Show more" click.
			this.fetchNextRecipes();
		}.bind(this));
	};

	api.onRegister = function(scope) {
		new RecipeListing(scope.$scope);
	};

	Cog.registerComponent({
		name: "recipeListing",
		api: api,
		selector: ".recipeListing"
	});
})(Cog.jQuery());
