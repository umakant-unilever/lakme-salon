(function($) {
	"use strict";

	var api = {};

	api.onRegister = function(element) {
		var $deepblueCaptchaContainer = element.$scope.find(".deepblue-captcha");
		var $brandElement = $("#brand");
		var brand;
		var deactivateErrorMsgDelay = 2000;

		function deactivateErrorMsg() {
			// TODO; replace this with a data-callback attribute
			if (window.grecaptcha && grecaptcha.getResponse && grecaptcha.getResponse() && element.$scope.find(".error-msg.active").length) {
				element.$scope.find(".error-msg.active").removeClass("active");
			}
			setTimeout(deactivateErrorMsg, deactivateErrorMsgDelay);
		}

		if ($deepblueCaptchaContainer.length && $brandElement.length) {
			brand = $brandElement.val();
			$.get("/sk-eu/services/deepblueCaptcha?brand=" + brand)
				.done(function(data) {
					$deepblueCaptchaContainer.html(data);
				})
				.fail(function() {
					$deepblueCaptchaContainer.html("Failed to obtain captcha image. Check the brand and public key.");
				});
		}

		deactivateErrorMsg();
	};

	Cog.registerComponent({
		name: "captcha",
		api: api,
		selector: ".captcha"
	});
})(Cog.jQuery());
