(function() {
	"use strict";

	var api = {};
	var classes = { 
		isHidden: "is-hidden",
		positionBottom: "position-bottom"
	};

	function ChannelAdvisor($el) {
		this.$btnBuyItNow = $el.children(".channelAdvisor-buy-it-now-btn");
		this.$retailersPopup = $el.children(".channelAdvisor-popup");
		this.$btnClosePopup = this.$retailersPopup.children(".channelAdvisor-close-btn");
		this.bindUIEvents();

		if (typeof this.$btnBuyItNow.offset() !== "undefined" && this.$btnBuyItNow.offset().top < this.$retailersPopup.height()) {
			this.$retailersPopup
				.addClass(classes.positionBottom)
				.css({
					top: $el.height()
				});
		}
	}

	ChannelAdvisor.prototype.bindUIEvents = function() {
		this.$btnBuyItNow.on("click", function() {
			this.$retailersPopup.toggleClass(classes.isHidden);
		}.bind(this));

		this.$btnClosePopup.on("click", function() {
			this.$retailersPopup.addClass(classes.isHidden);
		}.bind(this));
	};

	api.onRegister = function(scope) {
		new ChannelAdvisor(scope.$scope);
	};

	Cog.registerComponent({
		name: "channelAdvisor",
		api: api,
		selector: ".channelAdvisor"
	});
}());
