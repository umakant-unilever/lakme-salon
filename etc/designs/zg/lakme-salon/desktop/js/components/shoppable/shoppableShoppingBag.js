(function($) {
	"use strict";

	var api = {};
	var $el;
	var $itemList;
	var $price;
	var $emptyBagInfo;
	var templates = {};
	var currency;
	var shoppableToken;
	var config;
	var analyticsDef;
	var $title;
	var retry = 0;
	var maxRetry = 19;
	var retryDelay = 250;

	api.init = function(scope) {
		$el = $(scope);
		analyticsDef = this.external.eventsDefinition;
		analyticsSetup();

		if (!$el.hasClass("shoppable-shoppingbag")) {
			return;
		}

		shoppableToken = getShoppableToken();
		config = getConfig();
		$itemList = $el.find(".shopping-bag-item-list");
		$price = $el.find(".shopping-bag-price");
		$emptyBagInfo = $el.find(".shopping-empty-bag-info").text(config.emptyBagInfo);
		$title = $el.find("h2:first");
		templates = {
			item: doT.template($el.find(".item-template").text()),
			qtySelect: doT.template($el.find(".qty-select-template").text())
		};
		currency = $el.find(".shopping-bag-currency").text();

		bindUIEvents();
		refreshCart();
	};

	function refreshCart() {
		if (shoppableToken && "Cart" in window && "g_opt_obj" in window) {
			var cartId = Cart.find_cookie(shoppableToken).cartId;
			var showCart = false;

			// Shoppable doesn't return correct id on local environment.
			if (!cartId) {
				return;
			}
			try {
				Cart.get_cart(shoppableToken, cartId, showCart, function(error, data) {
					if (data.merchants && data.merchants.length) {
						$emptyBagInfo.hide();
						$el.removeClass("shoppingbag-empty");
					} else {
						$emptyBagInfo.show();
						// include class in the wrapper setting that is empty:
						$el.addClass("shoppingbag-empty");
					}

					buildCartItemsHTML(data);
					$price.text(formatCurrency(data.cart.subtotal) || 0);
				});
			} catch (e) {
				// console.log("Cart error", e);
			}
		} else if (shoppableToken && retry < maxRetry) {
			retry = retry + 1;
			setTimeout(refreshCart, retryDelay);
		}
	}

	function buildCartItemsHTML(data) {
		var merchants = data.merchants;
		var itemList = [];

		$.each(merchants, function(i, merchant) {
			var items = merchant.items;

			$.each(items, function(j, item) {
				itemList.push({
					name: item.name,
					sku: item.sku,
					upc: item.upc,
					imgSrc: item.images[0],
					merchant: merchant.name,
					size: item.size,
					price: currency + item.price,
					qty: item.qty,
					editLabel: config.editLabel,
					removeLabel: config.removeLabel,
					subtotal: currency + formatCurrency(item.subtotal),
					qtySelectHTML: getQtySelectHTML(item.qty)
				});
			});
		});

		$itemList.html(templates.item({
			itemList: itemList
		}));

		$title.attr("data-bag-quantity", itemList.length);
	}

	function getQtySelectHTML(currentQty) {
		var maxQty = 12;
		var options = [];

		for (var i = 1; i <= maxQty; i++) {
			options.push({
				value: i,
				selected: i === currentQty
			});
		}

		return templates.qtySelect({options: options});
	}

	function bindUIEvents() {
		$itemList.on("click", ".item-edit", function() {
			var mode = "updateProduct";
			var $item = $(this).parents(".shoppable-item");
			var sku = $item.data("sku");
			var upc = $item.data("upc");

			Product.pop_pdp(config.idType, upc, mode, sku);
		});

		$itemList.on("click", ".item-remove", function() {
			var sku = $(this).parents(".shoppable-item").data("sku");

			Product.delete_item(sku, refreshCart);
		});

		$itemList.on("change", ".select-item-qty", function() {
			var sku = $(this).parents(".shoppable-item").data("sku");

			Product.set_item_qty(sku, $(this).val());
		});

		// jQuery is available in global scope only if Shoppable is enabled.
		if (typeof jQuery !== "undefined") {
			if (jQuery.Topic) {
				jQuery.Topic("ADD_TO_CART").subscribe(refreshCart);
				jQuery.Topic("REMOVE_FROM_CART").subscribe(refreshCart);
			}
			Cog.fireEvent("shoppable", analyticsDef.OTHER.SHOPPABLE_CART_VIEW);
		}
	}

	function getShoppableToken() {
		var shoppableScript = $("#shoppable_bundle").get(0);

		if (!shoppableScript) {
			return null;
		}

		var scriptOptions = shoppableScript.attributes.options.value;
		var optionsJSON = JSON.parse("{" + scriptOptions + "}");

		return optionsJSON.token;
	}

	function getConfig() {
		return $el.find(".shopping-bag-config").data("config");
	}

	function analyticsSetup() {
		Cog.fireEvent("shoppable", analyticsDef.OTHER.SHOPPABLE_INIT);
	}

	function formatCurrency(value) {
		if (isNaN(value)) {
			return "0.00";
		}

		var returnValue = Math.round(value * 100) / 100;
		returnValue = returnValue.toString().split(".");
		if (returnValue.length === 1) {
			return returnValue[0] + ".00";
		}
		return returnValue[0] + "." + (returnValue[1] + "00").substr(0, 2);
	}

	Cog.register({
		name: "shoppableShoppingBag",
		api: api,
		selector: ".shoppable-shoppingbag",
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			}
		]
	});
})(Cog.jQuery());
