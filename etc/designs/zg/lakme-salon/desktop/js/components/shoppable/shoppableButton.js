(function() {
	"use strict";

	var api = {};
	var eventsDef = {};
	var utils = {};

	function ShoppableButton($addToBagBtn) {
		this.$button = $addToBagBtn;
		this.eanNumber = $addToBagBtn.data("ean");
		this.init($addToBagBtn);
		this.validate();
		this.bindUIEvents();
	}

	ShoppableButton.prototype = {
		init: function($addToBagBtn) {
			this.idType = $addToBagBtn.data("idtype");
			this.eanNumber = $addToBagBtn.data("ean");

			if (this.$button.closest(".quickview-container.is-active").length) {
				Cog.addListener("variantList", "variantChanged", function(e) {
					this.eanNumber = e.eventData.ean;
					this.$button.attr("data-ean", this.eanNumber);
				}.bind(this));
			}
		},
		validate: function() {
			if (typeof this.eanNumber === "undefined" || typeof this.idType === "undefined") {
				this.$button.attr("disabled", true);
			}
		},
		bindUIEvents: function() {
			this.$button.bind("click", function() {
				if (typeof Product !== "undefined") {
					Product.pop_pdp(this.idType, this.eanNumber, "addProduct");
				}
				Cog.fireEvent("shoppable", eventsDef.SHOPPABLE_BIN_CLICK, {
					componentPosition: utils.getComponentPosition(this.$button),
					product: this.eanNumber
				});
			}.bind(this));
		}
	};

	api.onRegister = function(scope) {
		var $el = scope.$scope;
		var	$addToBagBtn = $el.find(".addtobag-btn");
		eventsDef = this.external.eventsDefinition.CLICK;
		utils = this.external.utils;

		if ($addToBagBtn.length > 0) {
			new ShoppableButton($addToBagBtn);
		}
	};

	Cog.registerComponent({
		name: "shoppable",
		api: api,
		selector: ".shoppable",
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
}());
