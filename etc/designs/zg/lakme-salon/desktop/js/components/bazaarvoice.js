/**
 * BazaarVoice component - desktop/js/components/bazaarvoice.js
 */

(function($) {
	"use strict";

	var api = {};
	var analyticsDef;
	var analyticsUtils;

	function InlineRating() {
		analyticsSetup();
	}

	function analyticsSetup() {
		Cog.fireEvent("bazaarvoice", analyticsDef.OTHER.BAZAARVOICE);
	}

	api.onRegister = function() {
		analyticsDef = this.external.eventsDefinition;
		analyticsUtils = this.external.utils;

		if (typeof $BV !== "undefined") {
			new InlineRating();
		}
	};

	api.init = function() {
		function reloadInlineRatings() {
			var data = {
				productIds: [],
				containerPrefix: "BVRRInlineRating"
			};
			$(".inline-bv").each(function() {
				var id = $(this).data("product-id");
				data.productIds.push(id + "");
			});
			$BV.ui("rr", "inline_ratings", data);
		}

		if (typeof $BV !== "undefined") {
			if ($(".listingFilters").length) {
				Cog.addListener("listingFilters", "selectChanged", reloadInlineRatings);
			} else {
				reloadInlineRatings();
			}
		}
	};

	Cog.registerComponent({
		name: "bazaarvoice",
		api: api,
		selector: ".ratingsandreviews",
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})(Cog.jQuery());
