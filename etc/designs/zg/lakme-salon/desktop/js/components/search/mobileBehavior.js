(function($) {
	"use strict";
	function MobileBehaviour($el) {
		this.$el = $el;
		bindAll(this);
		this.init();
	}

	MobileBehaviour.prototype = {
		init: function() {
			enquire.register("only screen and (max-width: " + breakpoints.maxMobile + "px)", {
				match: this.matchMobile
			});

		},

		matchMobile: function() {
			var $navigationList = $(".search-results-wrapper .navigation-list");
			// could not find the navList
			if (!$navigationList.length) {
				return;
			}
			// if view all items is enabled.
			if ($navigationList.find(".is-active .view-all").length) {
				this.redirectToProducts();
				return;
			}

			// if is not product list
			if (!$navigationList.find(".is-active .view-products").length) {
				// hide buttons to change view
				$(".switchToListButton").hide();
				$(".switchToGridButton").hide();
				// force list view
				$(".searchResults .component-content").addClass("display-list").removeClass("display-grid");
			}
		},

		redirectToProducts: function() {
			var url = $(".search-results-wrapper .navigation-list .view-products").prop("href");
			if (url) {
				window.location = url;
			}
		}
	};

	var bindAll, breakpoints;
	var api = {
		onRegister: function(scope) {
			bindAll = bindAll || this.external.bindAll;
			breakpoints = breakpoints || this.external.breakpoints;
			new MobileBehaviour(scope.$scope);
		}
	};
	Cog.registerComponent({
		name: "search.mobileBehaviour",
		api: api,
		selector: ".searchTabNavigation",
		requires: [{
			name: "utils.bindAll",
			apiId: "bindAll"
		},{
			name: "utils.breakpoints",
			apiId: "breakpoints"
		}]
	});
})(Cog.jQuery());
