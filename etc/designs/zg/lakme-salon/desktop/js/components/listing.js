/**
 * Listing analytics elements
 */
(function($) {
	"use strict";

	var api = {};
	var analyticsUtils;
	var analyticsDef;
	var ctConstants;

	function Listing($el) {
		this.$el = $el;
		this.$items = this.$el.find(".listing-item");
		this.componentPosition = analyticsUtils.getComponentPosition($el);
		this.listingType = this.$el.data("itemtype") || "";
		this.$linkContainers = this.$el.find(".component");
		this.$articleLinks = this.$linkContainers.filter("[data-itemtype='article']").find("a");
		this.$productLinks = this.$linkContainers.filter("[data-itemtype='product']").find("a");
		this.$ctaLinks = this.$linkContainers.filter("[data-itemtype='cta']").find("a");
		this.$controls = this.$el.find(".carousel-controls");
		this.carouselImpressionInProgress = false;
		this.trackLinks();

		if (this.$el.find(".show-more").length) {
			this.trackImpressionOnLoad(this.$items.filter(":visible"));
		} else if ($el.hasClass("listing--product-variants")) {
			this.trackImpressionOnLoad(this.$items.first());
		} else if ($el.hasClass("listing--as-carousel")) {
			this.trackImpressionOnLoad(this.$items.filter(this.isProductVisible));
			this.trackCarouselImpression();
		} else {
			this.trackImpressionOnLoad(this.$items);
		}
		if (this.$el.is(".listing-product-default-view")) {
			this.setTileClicks();
		}
	}

	Listing.prototype = {

		trackImpressionOnLoad: function($products) {
			if (isDigitalDataActive && typeof digitalData !== "undefined") {
				digitalData.onLoadProductNames = digitalData.onLoadProductNames || [];

				$.each($products, function() {
					var ean = analyticsUtils.resolveListingProductEan($(this));
					if (allProducts[ean]) {
						var product = analyticsUtils.createProduct(allProducts[ean]);
						analyticsUtils.pushProduct(product);
						digitalData.onLoadProductNames.push(
								product.productInfo.productName);
					}
				});
			}
		},

		trackCarouselImpression: function() {
			this.$controls.on("click", function() {
				if (this.carouselImpressionInProgress) {
					clearTimeout(this.carouselImpressionInProgress);
				}
				this.carouselImpressionInProgress = setTimeout(function() {
					if (this.listingType === analyticsDef.ctConstants.product) {
						Cog.fireEvent("listing",
								analyticsDef.ctConstants.productImpression,
								{
									products: this.$items.filter(this.isProductVisible),
									componentPosition: this.componentPosition,
									componentName: analyticsDef.ctConstants.listing
								});
					}
					this.carouselImpressionInProgress = false;
				}.bind(this), 1000);
			}.bind(this));
		},

		trackLinks: function() {
			analyticsUtils.trackLinks(this.$articleLinks, {
				componentName: ctConstants.listing,
				componentPosition: this.componentPosition,
				type: ctConstants.article
			});
			analyticsUtils.trackLinks(this.$productLinks, {
				componentName: ctConstants.listing,
				componentPosition: this.componentPosition,
				type: ctConstants.product
			});
			analyticsUtils.trackLinks(this.$ctaLinks, {
				componentName: ctConstants.listing,
				componentPosition: this.componentPosition,
				type: ctConstants.cta
			});
		},

		isProductVisible: function() {
			return !$(this).hasClass("is-hidden");
		},

		setTileClicks: function() {
			this.$el.on("click", ".listing-item .box.last", function(e) {
				var $target = $(e.target);
				var $currentTarget = $(e.currentTarget);
				var $link;
				var href;
				if ($target.closest("a, button", $currentTarget).length === 0) {
					// if the event origin was NOT a link
					$link = $currentTarget.find(".richText-content a[href]:first");
					href = $link.attr("href");
					if ($link.length && href) {
						// if we can find a link with an href
						e.preventDefault();
						$currentTarget.css({"cursor":"wait"});
						location.href = href;
					}
				}
			});
		}
	};

	function addArticleClickListener($items) {
		var ENTER_CODE = 13;
		if ($items.length !== 0 && $items[0].dataset.itemPrimarykey.indexOf("article") !== -1) {
			$items.each(function() {
				var $item = $(this);
				var $target = $item.find(".content").first();
				$target.attr("tabindex", 0);
				$target.on("keyup", function(e) {
					if (e.keyCode === ENTER_CODE) {
						$target.trigger("click");
					}
				});
			});

			$items.on("click", "a", function(e) {
				onClickArticle(e);
			});
		}
	}

	function onClickArticle(e) {
		Cog.fireEvent("listingAnalytics", analyticsDef.CLICK.ARTICLE_LINK, {
			query: e.currentTarget.href
		});
	}

	api.onRegister = function(scope) {
		var $listing = scope.$scope;
		var $items = $listing.find(".listing-item");
		analyticsDef = this.external.eventsDefinition;
		ctConstants = this.external.eventsDefinition.ctConstants;
		analyticsUtils = this.external.utils;
		addArticleClickListener($items);
		new Listing(scope.$scope);
	};

	Cog.registerComponent({
		name: "listing",
		api: api,
		selector: ".listing",
		requires: [{
			name: "analytics.eventsDefinition",
			apiId: "eventsDefinition"
		},
		{
			name: "analytics.utils",
			apiId: "utils"
		}]
	});
})(Cog.jQuery());
