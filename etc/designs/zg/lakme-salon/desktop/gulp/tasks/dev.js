/* jshint node:true */

'use strict';

const gulp = require('gulp');

gulp.task('dev', ['concat','cache-styles'], function () {
	gulp.watch('sass/**/*.scss', ['styles']);
	gulp.watch(['js/**/*.js', '!js/head/*.js'], ['jshint:dev', 'jscs:dev']);
	gulp.watch(['js/head/*.js','!js/head/head.js'], ['concat']);
});
