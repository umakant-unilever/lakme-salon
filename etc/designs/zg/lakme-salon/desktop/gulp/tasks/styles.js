/* jshint node:true */

'use strict';

const path = require('path');
const gulp = require('gulp');
const handleErrors = require('../util/handleErrors');
const sass = require('gulp-sass');
const filter = require('gulp-filter');
const cached = require('gulp-cached');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const important = require('postcss-important-startstop');
const settings = require('../../package.json');
const rtl = require('postcss-rtl');
const sassInheritance = require('gulp-sass-inheritance');
const isRTL = process.env.npm_package_config_direction === 'rtl';
const cssnano = require('gulp-cssnano');
const cssnanoConfig = {
	discardUnused: false,   // This rule messes up with our icon font
	safe: true				// Stops cssnano rewriting z-index, animation-name etc... this causes havoc
};

gulp.task('cache-styles', function () {
	// task to cache files before watch (to avoid full build on first change)
	return gulp.src('sass/**/*.scss')
		.pipe(cached('sass'));
});

gulp.task('styles', function () {
	var postcssOptions = [
			autoprefixer({browsers: settings.browserslist}),
			important()
		];
	if (isRTL) {
		postcssOptions.push(rtl({onlyDirection: 'rtl'}));
	}

	return gulp.src('sass/**/*.scss')
		.pipe(cached('sass'))
		.pipe(sassInheritance({dir: 'sass/'}))
		.pipe(filter(function (file) {
			return !/^_/.test(path.basename(file.path));
		}))
		.pipe(sass().on('error', handleErrors))
		.pipe(postcss(postcssOptions))
		.pipe(cssnano(cssnanoConfig))
		.pipe(gulp.dest('css'))
});
