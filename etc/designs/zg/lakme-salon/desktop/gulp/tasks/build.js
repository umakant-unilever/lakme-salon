/* jshint node:true */

'use strict';

const gulp = require('gulp');

gulp.task('build', function() {
	return gulp.start('styles', ['jshint:build', 'jscs:build', 'concat']);
});
