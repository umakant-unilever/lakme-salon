/* jshint node:true */

'use strict';

const gulp = require('gulp');
const jscs = require('gulp-jscs');

gulp.task('jscs:dev', function() {
	return gulp.src(['js/**/*.js', '!js/polyfills/*.js', '!js/head/*.js', 'js/analytics/*.js', '!js/libs/*.js'])
		.pipe(jscs())
		.pipe(jscs.reporter());
});

gulp.task('jscs:build', function() {
	return gulp.src(['js/**/*.js', '!js/head/*.js', '!js/libs/*.js', 'js/analytics/*.js', '!js/polyfills/*.js'])
		.pipe(jscs())
		.pipe(jscs.reporter())
		.pipe(jscs.reporter('fail'));
});
